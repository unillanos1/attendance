package com.attendance.register.controllers;

import com.attendance.register.interfaces.ICustomerService;
import com.attendance.register.models.Customer;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@RequestMapping("customers")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CustomerController {

    private final ICustomerService customerService;

     @PostMapping
    private ResponseEntity<String> save(Customer customer){
        customerService.save(customer);
        return ResponseEntity.status(HttpStatus.CREATED).body("Customer saved");
    }

    @GetMapping
    private ResponseEntity<List<Customer>> findAll(){
        return  ResponseEntity.ok(customerService.getCustomers());
    }

}