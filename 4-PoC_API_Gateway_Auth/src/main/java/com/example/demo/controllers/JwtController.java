package com.example.demo.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.UsuarioModel;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
@RequestMapping("/jwt")
public class JwtController {
    private static final String SECRET = "mySecret";
    
    @PostMapping("/token")
    public String generarToken(@RequestBody UsuarioModel usuarioModel){
        return Jwts.builder()
                .signWith(SignatureAlgorithm.HS256, SECRET)
                .claim("body", usuarioModel.getUsername())
                .compact();
    }
}