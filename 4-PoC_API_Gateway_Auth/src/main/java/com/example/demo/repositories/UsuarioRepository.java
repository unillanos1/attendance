package com.example.demo.repositories;

import com.example.demo.models.UsuarioModel;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioModel, Long> {
    Optional<UsuarioModel> findById(Long userId); 
}
