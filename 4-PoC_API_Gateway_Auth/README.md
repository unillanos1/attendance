# Se adjuntan capturas de pantalla de la solucion en la carpeta assets/Evidencias


# Installing PostgreSQL on Ubuntu:
# Update the package list to get the most up-to-date information
sudo apt update

# Install PostgreSQL and related utilities
sudo apt install postgresql postgresql-contrib

# Start and enable PostgreSQL service
sudo systemctl start postgresql
sudo systemctl restart postgresql
sudo systemctl enable postgresql

# Check the status of PostgreSQL service
sudo systemctl status postgresql

Iniciar sesión como usuario postgres:
sudo -i -u postgres
psql

------------------------------------------------------------------
# Installing Kong on Ubuntu:

# Add Kong's official package repository
echo "deb [trusted=yes] https://bintray.com/kong/kong-deb `lsb_release -sc` main" | sudo tee -a /etc/apt/sources.list

# Add the GPG key for the Kong packages
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 40976EAF437D05B5

# Update the package list to get the most up-to-date Kong package information
sudo apt update

# Install Kong
snap install kong

# Start Kong service
sudo systemctl start kong

# Check the status of Kong service
sudo systemctl status kong

# Iniciar Kong
sudo snap start kong

# Detener Kong
sudo snap stop kong

# Verificar el estado del servicio Kong:
sudo snap services kong

--------------------------------------------------------------------------
# Configure PostgreSQL as Kong's Database
Update your Kong configuration file (kong.conf) to use PostgreSQL as the database:

sudo mkdir -p /etc/kong/
sudo nano /etc/kong/kong.conf

# Archivo kong.conf para configurar la conexión a PostgreSQL

# Tipo de base de datos (en este caso PostgreSQL)
database = postgres

# Host de la base de datos PostgreSQL
pg_host = localhost

# Puerto de la base de datos PostgreSQL
pg_port = 5432

# Usuario de PostgreSQL
pg_user = postgres

# Contraseña de PostgreSQL
pg_password = root

# Nombre de la base de datos en PostgreSQL que se usará para Kong
pg_database = postgres

---------------------------------------------------------
# Servicio

curl -i -X POST http://localhost:8001/services/ \
  --data "name=my-konga" \
  --data "url=http://localhost:8080/usuario/usuarios/"



curl -i -X POST http://localhost:8001/services/ \
  --data "name=my-id-konga" \
  --data "url=http://localhost:8080"



# Ruta
curl -i -X POST http://localhost:8001/services/my-konga/routes \
  --data "paths[]=/usuario/usuarios/"


curl -i -X POST http://localhost:8001/services/my-id-konga/routes \
  --data "paths[]=/usuario/usuario/{id}" \
  --data "strip_path=true" \
  --data "methods[]=GET"

---------------------------------------------------------
# Configuracion de plugin JWT en Kong Api Gateway

curl -i -X POST http://localhost:8001/services/my-konga/plugins/{e97730a5-4f82-4b37-b4de-958722eaf58f}/config \
  --data "key=jwt_secret" \
  --data "value=your_jwt_secret_key"

curl -i -X POST http://localhost:8001/services/my-konga/plugins/{e97730a5-4f82-4b37-b4de-958722eaf58f}/config \
  --data "key=claims_to_verify" \
  --data "value=exp,iss"

curl -i -X POST http://localhost:8001/services/my-konga/plugins/{e97730a5-4f82-4b37-b4de-958722eaf58f}/config \
  --data "key=uri_param_names" \
  --data "value=jwt"
----------------------------------------------------------
# RUN PROJECT

mvn clean install
mvn spring-boot:run


# Consultar por id
http://localhost:8080/usuario/usuario/1/

# Listar todos los usuarios
http://localhost:8080/usuario/usuarios/


http://localhost:8080/jwt/token/

{
    "username": "jhon",
    "password":"ikkjhm"
}